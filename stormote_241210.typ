#set text(font: "New Computer Modern", size: 11pt, lang: "sv")

#let header = {
  set text(size: 11pt, weight: "medium")
  grid(
    columns: (auto, auto, 1fr),
    image(height: 1cm, "lithekod.png"),
    [],
    align(right, text[Höstmöte\ Linköping 10 december 2024]),
  )
  v(-0.3cm)
  line(length: 100%)
}

#let sign-size = 1.0cm
#let footer = {
  set text(size: 11pt, weight: "medium")
  line(length: 100%)
  v(-0.1cm)
  grid(
    columns: (1fr, auto, 1fr),
    [],
    align(center, locate(loc => [
      #v(1.0em)#counter(page).display(both: true, "1 (1)")
    ])),
    align(right,
     [#box(width: sign-size*1.2, height: sign-size, stroke: (left: 1pt, top: 1pt, bottom: 1pt))#h(-4pt)
      #box(width: sign-size*1.2, height: sign-size, stroke: (left: 1pt, top: 1pt, bottom: 1pt))#h(-4pt)
      #box(width: sign-size*1.2, height: sign-size, stroke: (left: 1pt, top: 1pt, bottom: 1pt))#h(-4pt)
      #box(width: sign-size*1.2, height: sign-size, stroke: (left: 1pt, top: 1pt, bottom: 1pt, right: 1pt))]
    )
  )
}

#set page(
  paper: "a4",
  margin: (top: 4cm, bottom: 3.5cm, x: 3.5cm),
  header: header,
  footer: footer,
)

#align(center)[
  #text(size: 24pt)[LiTHe kod] \
  #text(size: 12pt)[Höstmöte 2024]

  #text(size: 12pt)[Sal: Ada Lovelace] \
  #text(size: 12pt)[10 december 2024]
]

#outline(indent: auto)

*Bilagor*

#context { enum(numbering: "A", indent: -2pt, ..query(<appendix>).map(it => {
it; box(width: 1fr, repeat[.]); [#counter(page).at(it.location()).at(0)] })) }

#set heading(numbering: "1.1")
#show heading.where(level: 1): it => {
  set text(size: 14pt)
  [#sym.section#numbering("1.1", ..counter(heading).at(it.location())) #it.body]
}
#show heading.where(level: 2): it => {
  set text(size: 11pt)
  it
  v(2pt)
}

#let qa(q, a) = [
  _Fråga_: #q \
  _Svar_: #a
]

#let boxed(..content) = {
  let content = content.pos()
  box(
    stroke: 1pt,
    inset: 0.5em,
    width: if content.len() > 1 { 100% } else { auto },
    content.at(0),
  )
  if content.len() > 1 {
    v(-0.75em)
    align(right, content.at(1))
  }
}

#let above-line = {
  v(0.5em)
  line(stroke: 0.5pt, length: 100%)
  v(-0.5em)
}

#pagebreak()

= Mötets öppnande

Mötet öppnas klockan 17:40 av Morgan Nordberg.

== Val av mötesordförande

Morgan kandiderar.

Mötet *beslutar* att välja Morgan till mötesordförande.

== Val av mötessekreterare

Morgan nominerar Johannes Kung.

Mötet *beslutar* att välja Johannes till mötessekreterare.

== Val av justerare tillika rösträknare

Morgan nominerar Astrid Lauenstein och Kacper Uminski.

Mötet *beslutar* att välja Astrid och Kacper till mötets justerare tillika
rösträknare.

== Fastställande av röstlängden

Röstlängden *fastställs* till 11  personer. 8 person utan rösträtt.

== Beslut om mötets stadgeenliga utlysande

Kallelse och preliminär föredragningslista har anslagits 4 veckor i förväg i enligthet med stadgarna. Föredragningslistan fastslogs en vecka innan stormötet utan ändringar.

= Föregående verksamhetsår

== Styrelsens verksamhetsberättelse

Morgan presenterar verksamhetsberättelsen såsom tagen ur senaste stormötes protokoll.

- Målade märkesbacken
- Deltog i STEN/STYFEN (kommer inte ihåg namnet)
- Många kom på första meetupen, jättekul
- NCPC gick bra, 10 lag deltog, jättekul
- Vi skickade ett lag till NWERC till slut, tack Emanuel
- Påbörjat föreningsrummet, behöver mer jobb under nästa år
- Mindroad-workshop om slump-algoritmer med slump
- Advent of Code
- Julstuga med Opera
- Lödöl med Mindroad
- Pratade med nya företag, men inga nappade på spons
- 3 game jams
- FPGA-kväll
- IMPA har pågått under hela året

== Styrelsens ekonomiska berättelse

Morgan presenterar den ekonomiska berättelsen såsom tagen ur senaste stormötets protokoll. Den ekonomiska berättelsen läggs
till handlingarna som @ekonomisk-berättelse.

== Revisorns preliminära granskning av verksamhetsåret

Frans Skarman, revisor för föregående verksamhetsår, läser upp granskningen av föregående verksamhetsåret såsom nedskriven i senaste stormötesprotokoll.
Revisionsberättelsen läggs till handlingarna som @revisionsberättelse.

Kommentarer:
- Nuvarande styrelsen har funderat på hur en överlämning till nästa styrelse kan ske på ett bra sätt
- Det kan behövas någon valförberedelse för att se till att den utvalda styrelsen vet vad som förväntas och kan förväntas hålla sina plikter
- Att saldot på banken låg nere på 13k kr beror på att faktureringen av sponsorer skedde sent i verksamhetsåret
  - Nuvarande styrelsen bör sikta på att fakturera sponsorerna tidigare under verksamhetsåret

= Beslut om ansvarsfrihet av föregående verksamhetsårs styrelse

Morgan föreslår att ansvarsbefria föregående versamhetsårets styrelse.

Mötet *beslutar* att ansvarsbefria föregående verksamhetsårs styrelse.

= Val av game jam-ansvarig

Styrelsen nominerar Martin Högstedt att bli inröstad till game jam-ansvarig som styrelsepost.

Martin presenterar sig själv och lämnar salen för röstningen.
Närvarande publik konstaterar att senaste game jam lett av Martin var roligt.

Mötet *beslutar* att välja in Martin till game jam-ansvarig och Martin kommer tillbaka in i salen.

= Motioner och propositioner

Inga inkomna.

= Övriga frågor

== Förslag från Lysator
Lysators ordförande Samuel Junesjö kommer med förslag om delade lokaler i HusEtt. B-huset har haft brist på lokaler med delar som ska byggas om. Det finns planer på att renovera bort café Java och det är oklart hur café Java kommer att utformas. Lysator har en lokal i HusEtt och Samuel föreslår en stor gemensam lokal för alla dataföreningar. 

Varje rum som Lysator hyr är ungefär 20 m². För varje rum som hyrs av en förening fås en rabatt. Rabatten minskar gradvis ju mer yta som hyrs. Det kan finnas möjlighet att riva väggar mellan rummen för vilket fall det är oklart hur rabatten påverkas. Samuel föreslår att Lysator står för eventuella rivnadskostnader alternativt att kostnaden delas på mellan föreningarna. Den ansvariga för Kårservice behöver kontaktas för detaljer om rivning av väggar.

Morgan och övriga i Lithe kods styrelse konstaterar att föreningens rum i Kårallen inte nyttjas särskilt i dagsläget. Få av föreningenings medlemmar känner till rummet och det används bara för lunchmöten. Det finns ett ledigt rum till höger om S-studenter som i sin tur ligger till höger om Lysators ena rum. Samuel föreslår att förhandla med föreningen S-studenter för att byta deras rum i HusEtt mot rummet i Kårallen eller på annat sätt få S-studenters rum. Alternativt föreslår Samuel att förhandla med LiU Water and Wind.

Samuel siktar på att med en större delad lokal erbjuda gemensam hängyta, gemensam hårdvaruyta med hårdvara från sponsorer, gemensamma arbetsstationer och liknande. Lokalen/lokalerna skulle ha ett system för ge föreningarnas medlemmar fri tillgång. Morgan och Samuel konstaterar att Lithe kods fikavagn kan förvaras i HusEtt för att slippa bära fikavagnen långt för meetups.

Lysator bjuder in till mer konversationer angående förslaget om styrelsen finner det intressant.

= Mötets avslutande

Mötet avslutas klockan 18:30 av Morgan Nordberg.

#let signature(post, name) = [#v(2cm)#line(length: 6cm, stroke: 0.5pt)#text(weight: "bold", post)\ #name]
#grid(
  columns: 2,
  rows: 2,
  column-gutter: 1fr,
  align(left, signature("Mötesordförande", [Morgan Nordberg])),
  align(right, signature("Mötessekreterare", [Johannes Kung])),
  align(left, signature("Justerare", [Astrid Lauenstein])),
  align(right, signature("Justerare", [Kacper Uminski])),
)

#set heading(numbering: "A.1", supplement: [Bilaga], outlined: false)
#counter(heading).update(0)
#show heading.where(level: 1): it => {
  pagebreak(weak: true)
  text(
    weight: "bold",
  )[Bilaga #numbering(it.numbering, ..counter(heading).at(it.location())): #it.body <appendix>]
}

#show heading.where(level: 2): it => {
  set heading(numbering: none)
  it
}

= Ekonomisk berättelse styrelseår 2023-2024 <ekonomisk-berättelse>

== Intäkter

#table(
  columns: (auto, auto),
  align: (left, right),
  [Ingående saldo], [148 153,89 kr],
  [Spons], [115 000,00 kr],
  [Medlemsavgift], [180,00 kr],
  [Totala intäkter], [115 230,00 kr],
  [Utgående saldo], [126 654,89 kr],
)

== Utgifter

#table(
  columns: (auto, auto),
  align: (left, right),
  [Game Jam-gruppen], [25 920,74 kr],
  [Hårdvarugruppen], [945,00 kr],
  [Kvarblivet 22-23], [10 529,11 kr],
  [Hyror], [23 086,97 kr],
  [Ledningströjor], [11 521,00 kr],
  [Meetup], [6 510,45 kr],
  [Nolle-P], [6 582,00 kr],
  [Tävlingsgruppen], [30 117,26 kr],
  [Övrigt], [21 606,47 kr],
  [Totala utgifter], [136 819,00 kr],
)

== Resultat

Under året har vi fått in 9 st nya medlemmar. Budgeten för förra året låg på
165 tkr, vi spenderade 136 tkr. Ursprungliga budgeten hade tänkt att
LiU-challenge skulle vara en grej, men det blev aldrig av. Totalt spenderade vi
28 tkr mindre än förväntat. Eftersom det ursprungligen var budgeterat att vi
skulle få mer intäkter av LiU-challenge så är detta rimligt.

= Revisionsberättelse styrelseår 2023-2024 <revisionsberättelse>

Ekonomin ser ut att ha skötts bra. Bokföringen stämmer med banktransaktioner, bortsett från några nyinkomna transaktioner från senaste dagarna som inte är införda än. Saldot på banken var nere på 13k kr innan sponsorpengarna kom in igen, vilket får mig att tänka det kanske börjar bli dags att bryta traditionen att sluta budgetera för förlust, ifall en sponsor skulle dra sig ut plötsligt.

Styrelsens arbete som helhet började starkt, men trappade av gradvis under året till den grad att det från mitt perspektiv var osäkert om föreningen skulle ta sig igenom året, vilket är tråkigt att se. Jag vet dock att det finns personliga anledningar bakom det och att styrelsearbetet har upplevts som rätt krävande så jag ser ingen anledning att inte ge ansvarsfrihet, utan undrar mest om föreningen kan ha något system för att undvika liknande problem i framtiden.

= Budget 24/25 <budget>

#v(2em)

*Intäkter*

#table(
  columns: 2,
  align: (left, right),
  stroke: none,
  table.hline(stroke: 1pt),
  [*Post*],
  [*Budgeterat (kr)*],
  table.hline(stroke: 0.5pt),
  [Spons Opera], [50 000],
  [Spons Axis], [40 000],
  [Spons Mindroad], [25 000],
  [Medlemsavgifter], [1 000],
  [*Totalt*], [116 000],
  table.hline(stroke: 1pt),
)

#v(1em)
*Utgifter*

#table(
  columns: 2,
  align: (left, right),
  stroke: none,
  table.hline(stroke: 1pt),
  [*Post*],
  [*Budgeterat (kr)*],
  table.hline(stroke: 0.5pt),
  [Styrelsetröjor], [10 000],
  [Meetup], [10 000],
  [Styrelsen till förfogande], [10 000],
  [NCPC], [12 000],
  [IMPA], [36 000],
  [Game Jam x3], [30 000],
  [Advent of Code], [5 000],
  [Lokalhyra], [16 000],
  [Förrådshyra], [6 000],
  [Stormöten], [4 000],
  [*Totalt*], [139 000],
  table.hline(stroke: 1pt),
)
