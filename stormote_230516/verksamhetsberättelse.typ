// vim: filetype=

#set page(numbering: "1 (1)")

= Föregånde verksamhetsårs styrelses verksamhetsberättelse

LiTHe kod har under verksamhetsåret 22/23 anordnat och samordnat ett antal
evenemang. 

Verksamhetsåret började med nolleperioden där vi deltog i Y-arnas, D-arnas samt
Lingarnas nollebok. LiTHe kod var också delaktiga i fångarna på STYFEN med egen
station. Detta ledde sedan till en större första meetup med nya intressenter.
Meetups har sedan fortsatt varje tisdag, med undantag från vissa perioder,
under hela läsåret. LiTHe kod har under året tecknat avtal och varit
samarbetspartner med Mindroad, Opera, Ericsson och Axis. Alla dessa har
huvudsakligen sponsrat tävlingsprogrammering i syfte att marknadsföra sig
själva. Utöver det ges var och en av företagen chansen att i samarbete med
LiTHe kod anordna ett event.

- Opera: Julstuga
- Mindroad: Lödöl
- Ericsson: Blankt, några utskick mest.
- Axis: Mer blankt

Tävlingsprogrammering har haft IMPA (lius interna mästerskap i programmering
och algoritmer) körandes under hela året. Deltagandet har varit relativt lågt,
mest vanligen 5-10 personer per omgång. Har testat lite nytt här med och drivit
write ups till problemen under första terminen, vilket inte märkbart påverkade
deltagandesiffran, eller respons från tagare. NCPC - oktober runt tio lag som
deltog (lag om tre) där topp två lag skickades vidare till NWERC som detta år
hostades i Delft, Nederländerna. Lag Gekko Kickers och Dal Dal Doma reste ner
till Nederländerna och slutade på platser omkring 100 av runt 200 deltagande
lag.

- Advent of Code - högt deltagande

Nytt för i år är att LiU Challenge återupplivades i Swedish Coding Cup, där
problemskrivare från Liu får vara med och anordna en deltävling på kattis. I
denna tävling deltog omkring 40 personer.

Ytterligare har LiTHe kod anordnat en git-föreläsning tillsammans med Donna, och tre game jams.

- Fall
- Global
- Spring

Införskaffat lokal i Kårallen.
