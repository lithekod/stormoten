// vim: filetype=

#set heading(numbering: "1.1.")
#set page(numbering: "1 (1)")

#align(center)[
#text(size: 20pt)[LiTHe kod vårmöte 2023]#linebreak()#v(0.2em)
#text(size: 14pt)[Ada Lovelace #linebreak() 2023-05-16]
]

= Mötets öppnande

Styrelsens ordförande Lowe Kozak förklarar mötet öppnat 18:24.

= Val av mötesordförande

Lowe nominerar sig själv. Inga motsättningar.

= Val av mötessekreterare

Lowe nominerar Gustav Sörnäs. Inga motsättningar.

= Val av justeringsperson tillika rösträknare

Lowe nominerar Hannah Bahrehman och Victor Lells. Inga motsättningar.

= Fastställande av röstlängden

*Beslut* Röstlängden fastställs till 14.

= Beslut om mötest stadgeenliga utlysande

Lowe går igenom vad stadgarna säger om stormötets utlysande.

*Beslut* Mötet är stadgeenligt utlyst.

= Styrelsens verksamhetsberättelse

Lowe läser upp verksamhetsberättelsen för verksamhetsåret 22/23. Verksamhetsberättelsen läggs till handlingarna.

Fråga: Hur många lag deltog i NWERC i Delft? #linebreak()
Svar: 200 lag ungefär.

(Inte sist!)

= Styrelsens ekonomiska berättelse

Styrelsens kassör Emanuel Särnhammar läser upp den ekonomiska berättelsen för verksamhetsåret 22/23. Den ekonomiska berättelsen läggs till handlingarna.

Kommentar: NCPC-posten räknar in NWERC-resan.

Fråga: Vad är "Styrelsen till förfogande"? #linebreak()
Svar: I praktiken, allt övrigt.

Följdfråga: Är inte det lite skumt? #linebreak()
Svar: Jo, det är redan ändrat i nästa års budgetförslag.

= Revisorns preliminära granskning av verksamhetsåret

Föreningens revisor Victor Lells läser upp den preliminära granskningen.

#box(stroke: 1pt, inset: 1em)[
_Ja, ser bra ut._
]

= Proposition: Testa och utvärdera att införa undergrupper för föreningens verksamhet för nästa verksamhetsår

Gustav läser upp propositionen.

Fråga: Till vilken grad förväntas styrelsen detaljstyra undergruppernas verksamhet? #linebreak()
Svar: Inte uppenbart, behöver testas och utvärderas under året. Det är oavsett viktigt med en öppen dialog mellan styrelse/undergrupper.

Kommentar: Kassören skulle fortfarande göra mycket jobb, med att springa runt med bankkort, göra stora överföringar och liknande.

Förslag: Undersök möjligheten att delegera bankansvaret. Mini-kassörer? #linebreak()
Svar: En fråga för nästa styrelse.

Fråga: Vad är problemet som styrelsen försöker lösa med den här propositionen? #linebreak()
Svar: Det är ont om aktiva personer inom föreningen (som anordnar verksamhet) och en teori är att det känns som mycket ansvar att anta sig, så personer som hade kunnat anordna verksamhet avstår. Undergrupper gör att man kan sprida ut ansvaret över flera personer, men fortfarande ha förtroendevalda gruppledare.

Följdfråga: Löser den här propositionen problemet? #linebreak()
Svar: Martin som föreslagits som gruppledare för LiU Game Jam hade inte velat sitta som Game Jam-ansvarig i styrelsen av nämnda anledningen. Gustav som föreslagits som gruppledare för hårdvarugruppen har en liknande åsikt, men där handlar det om att anordna verksamhet som föreningen i nuläget inte anordnar.

Fråga: Vad händer med en undergrupps budget om gruppledarposten är vakantsatt? #linebreak()
Svar: Budgeten lämnas som den är. Antingen hittar styrelsen en person senare under året, eller så använder styrelsen budgeten för det budgeterade syftet. Styrelsen kan ses som ovanför undergrupper och spenderar pengar "för" undergrupper, vid behov, medan en gruppledare som regel enbart spenderar pengar inom sin budgetpost.

*Beslut (19:08)* Mötet adjungeras till 19:25.

Lowe återupptar mötet 19:27. Röstlängden justeras till 13.

*Beslut* Mötet antar linje 2 i propositionen.

= Personval

== Föreningens styrelse för verksamhetsåret 23/24

_Vid varje votering lämnade vederbörande mötet. Röstlängden justerades ner till
12 inför varje beslut och sedan upp till 13._

=== Val av styrelsens ordförande

Styrelsen nominerar Henry Andersson.

*Beslut* Henry Andersson väljs till styrelsens ordförande.

=== Val av styrelsens vice ordförande

Styrelsen föreslår att vakantsätta rollen som styrelsens vice ordförande.

*Beslut* Styrelens vice ordförande vakantsätts.

=== Val av styrelsens kassör

Styrelsen nominerar Emanuel Särnhammar.

*Beslut* Emanuel Särnhammar väljs till styrelsens kassör.

=== Val av styrelsens verksamhetsledare

Styrelsen nominerar Hamza Keifo.

*Beslut* Hamza Keifo väljs till styrelsens verksamhetsledare.

=== Val av styrelseledamöter

Styrelsen nominerar Hannah Bahrehman.

*Beslut* Hannah Bahrehman väljs till styrelseledamot.

Simon Gutgesell nominerar sig själv.

*Beslut* Simon Gutgesell väljs till styrelseledamot.

== Gruppledare för föreningens undergrupper för verksamhetsåret 23/24

Styrelsen nominerar följande personer:

- Martin Högstedt som gruppledare för LiU Game Jam.
- Gustav Sörnäs som gruppledare för hårdvarugruppen.
- Simon Gutgesell som gruppledare för WWW-gruppen.
- Emanuel Särnhammar som gruppledare för meetupgruppen.
- Hannah Bahrehman som gruppledare för lokalgruppen.
- Lowe Kozak som gruppledare för tävlingsprogrammeringsgruppen.

*Beslut* Styrelsens nomineringar till gruppledare väljs in.

== Val av föreningens revisor

Röstlängden justeras till 14.

Styrelsen nominerar Frans Skarman till föreningens revisor.

*Beslut* Frans Skarman väljs till föreningens revisor.

Röstlängden justeras till 13.

= Fastställande av föreningens budget

Budgeten presenteras och läggs till handlingarna.

*Beslut* Den föreslagna budgeten antas.

= Fastställande av föreningens medlemsavgift

Styrelsen föreslår att medlemsavgiften behålls på den nuvarande 20 kr för nya
medlemmar och 0 kr för nuvarande medlemmar.

Fråga: Borde föreningen gå över till en mer medlemsfinansierad modell? #linebreak()
Svar: Kanske senare, men inte i år.

*Beslut* Medlemsavgiften fastställs till 20 kr för nya medlemmar och 0 kr för nuvarande medlemmar.

= Motioner och propositioner

Inga inkomna.

= Övriga frågor

Inga övriga frågor.

= Mötet avslutas

Mötet avslutas 20:04.

#table(
    columns: (1fr, 1fr),
    stroke: none,
    inset: 0pt,
    gutter: 0.5em,
    v(4em), [],
    [*Mötesordförande*], [*Mötessekreterare*],
    [Lowe Kozak], [Gustav Sörnäs],
    v(4em), [],
    [*Justeringsperson*], [*Justeringsperson*],
    [Hannah Bahrehman], [Victor Lells],
)
