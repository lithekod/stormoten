// vim: filetype=

#set page(numbering: "1 (1)")

= Styrelsens föreslagna budget

_Med en asterisk menas att ytterligare inkomster och utgifter är väntade innan verksamhetsårets slut._

```
(kr)
=================================================
Intäkter              Budget 23/24 |       22/23
-----------------------------------+-------------
Intäkter                           |
Spons                   140 000,00 |  160 000,00
Medlemsavgifter           1 000,00 |      640,00
Totalt                  141 000,00 |  160 640,00
                                   |
-----------------------------------+-------------
Utgifter
-----------------------------------+-------------
Marknadsföring           10 000,00 |   14 832,00
Styrelsetröjor            6 000,00 |    8 310,00
Undergrupper                       |
  Meetup                 10 000,00 |    3 741,23
  Game Jam               35 000,00 |   44 327,93
  Tävlingsprogrammering            |
    NCPC                 12 000,00 |    9 344,20
    IMPA                 36 000,00 |   11 600,00*
    LiU Challenge        10 000,00 |    5 000,00*
    NWERC                     0,00 |   47 297,34
    AoC                   4 000,00 |    1 991,59
  Föreningsrum           15 000,00 |        0,00
  Hårdvarugruppen         5 000,00 |        0,00
Förråd                    6 000,00 |    8 587,00 
Stormöte                  4 000,00 |            *
10++-jubileum             5 000,00 |        0,00
Övrigt                    2 000,00 |            *
Rest                          -,-- |   11 800,00
Totalt                  160 000,00 |  184 100,55*
-----------------------------------+-------------
Resultat               - 19 000,00 | - 23 460,55*
=================================================
```
