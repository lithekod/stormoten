= LiTHe kods ekonomiska berättelse - 2023-11-21

== Intäkter

#table(
  columns: (auto, auto),
  [Ingående saldo], [`186 627,26 kr`],
  [Spons], [`160 000,00 kr`],
  [Medlemsavgift], [`    640,00 kr`],
  [Totala intäkter], [`160 640,00 kr`],
  [Utgående saldo], [`147 753,89 kr`],
)

== Utgifter

#table(
  columns: (auto, auto),
  [Förra styrelseåret], [` 11 800,00 kr`],
  [Game Jam], [` 47 875,93 kr`],
  [Marknadsföring], [`  8 310,00 kr`],
  [NCPC], [` 56 641,54 kr`],
  [Nolle-P], [` 14 832,00 kr`],
  [Priser], [` 27 283,58 kr`],
  [LiU-challenge], [`  2 542,95 kr`],
  [Styrelse till förfogande], [` 33 893,02 kr`],
  [Totala utgifter], [`203 179,02 kr`],
)

= Resultat

Under året har vi fått in 32 st nya medlemmar vilket är en bra ökning.
Budget för året var ursprungligen 230 tkr men eftersom vi tappade IDA Infront som sponsor (-40 tkr) ledde det till att styrelsen ville vara lite mer konservativ för att kompensera.
Tack vare att Lowe höll LiU-challenge i år fick vi ytterligare 20 tkr extra i spons.
Ursprungligt var det budgeterat en förlust på 40 tkr och kommer i nuläget ligger vi -42 tkr.
