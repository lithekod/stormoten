#set heading(numbering: "1.1.")
#set page(numbering: "1 (1)")

#align(center)[
#text(size: 20pt)[LiTHe kod höstmöte 2023]#linebreak()#v(0.2em)
#text(size: 14pt)[Ada Lovelace #linebreak() 2023-11-21]
]

= Mötets öppnande

Styrelsens ordförande Lowe Kozak förklarar mötet öppnat 18:18.

= Val av mötesordförande

Henry Andersson nominerar sig själv. Inga motsättningar.

= Val av mötessekreterare

Henry nominerar Emanuel Särnhammar. Inga motsättningar.

= Val av justeringsperson tillika rösträknare

Henry nominerar Hannah Bahrehman och Hamza Keifo. Inga motsättningar.

= Fastställande av röstlängden

*Beslut* Röstlängden fastställs till 9.

= Beslut om mötest stadgeenliga utlysande

Mötet annonserades på hemsidan fyra veckor innan mötet med föredragningslistan fastställd tidigare än en vecka innan mötet.
Mötet annonserades i Discord två veckor innan mötet.
Allt detta skedde i enlighet med stadgarna.

*Beslut* Mötet är stadgeenligt utlyst.

= Styrelsens verksamhetsberättelse

Röstlängd justeras till 10 personer.

Henry läser upp föregående årets verksamhetsberättelse.

= Styrelsens ekonomiska berättelse

Mötet föreslår att förflytta punkten.

= Revisorns gransknings av föregående verksamhetsår

Victor Lells har kollat igenom bokföringen och verifierat mot bankutdrag, inga konstiga utgifter fanns.
Victor kommenterade att NWERC-resan blivit dyr, men sade att det blir så när man ska åka utomlands.

= Föregående verksamhetsårets ekonomiska berättelse

Emanuel presenterade det föregående styrelseårets ekonomiska berättelse.

Frans Skarman undrade vad det budgeterade underskottet för året varit. Underskottet var planerat på 44 tusen kronor och det verkliga underskottet blev 42 tusen kronor.

Henry undrar vad som mindre pengar spenderades på. Victor nämnde att inget jubileum hade hållits och Frans nämnde att mindre att spenderats under _styrelsen till förfogande_-posten. Det komms också fram till att Nolle-P hade varit billigare än förväntat och att alla budgeterade IMPA-priser inte hade hämtats ut.

= Beslut om ansvarsfrihet av föregående verksamhetsårs styrelse

Ingen emot, föregående styrelsen är ansvarsbefriad.

= Motioner och propositioner

Inga inkomna.

= Övriga frågor

== LiTHe kods styrelse har gjort en budgetrevidering

LiTHe kods styrelse föreslår en förändrad budget för året.
Ändringarna skulle innebära att förflytta 4 tkr från Nolle-P-posten till posten för styrelsetröjor.
Förslaget innebar också en ökning av lokalgruppens budget med 10 tkr och en sänkning av jubileumsposten samt hårdvarugruppens poster med 2.5 tkr var. Den totala skillnaden är 5 tkr högre utgifter än tidigare budget.

Mötet förklarade att lokalgruppen tidigare inte hade haft en budget utöver hyra, vilket är motiveringen till en höjning i lokalgruppens budget. Det förklarades också att styrelsen inte hade ork eller var motiverad till att planera ett jubileum, istället diskuterades en större meetup, vilket motiverar halveringen i jubileumets budgetpost. Hårdvarugruppen har inte spenderat några pengar ännu, därför sänks budgeten med hälften.

Två potentiella sponsorssamarbeten är del av den nya budgetrevideringen: SICK och LiU-challenge med Mindroad. SICK är en ny potentiell sponsor, inget kontrakt har skrivits. Mindroad lät taggade på LiU-challenge, men där saknades också kontrakt.

En tanke som mötet framlägger är att det kanske är konstigt att ändra budgetposter som använts färdigt. En kommentar från mötet är att budgeten borde reflekterra vad som faktiskt spenderas.

En annan fråga är varför det blev dyrare med styrelsetröjor trots att det var en mindre styrelse än tidigare år. Förklaringen som gavs är att styrelsen var mindre men varje gruppledare för respektive undergrupp fick en ledningströja. En följdfråga var varför det inte budgeterades i förväg. Det motiverades att det inte hade beslutats om undergrupper skulle få tröjor när budgeten skapades.

En fråga framfördes om vad den tidigare budgeten för lokalgruppen var. Tidigare var dess budget 0 kr.

En annan fråga var varför Opera betalade mer än Axis. Anledningen till detta är att Henry mejlade Opera och bad om mer, vilket de gick med på; AXIS var inte villiga att öka sponsringen.

Martin Högstedt frågar om lokalgruppens nya budgetpost är pengar som redan spenderats eller om det är pengar som planeras att spenders. Hittills har \~4 600 kr spenderats med 10 tkr som en övre gräns i budgeten.

En formell fråga om revideringen var om motionen fanns tillgänglig tidigare. Det gjorde den inte.

Det röstas om att lägga till revideringen som ett ärende. 10 röstar för, vilket innebär att det läggs till.

Det röstas om motionen. 10 röstar för, det går igenom.

== Undergruppsutvärderingar

=== Gustav Sörnäs - Hårdvarugruppen

Gustavs meddelande läses upp.

=== Martin Högstedt - Game jam-gruppen

Martin läser upp sin utvärdering.

=== Lowe Kozak - Tävlingsprogrammeringsgruppen

Lowe Kozak var frånvarande från mötet och hade inte skickat in någon utvärdering för mötet att läsa.

=== Hannah Bahrehman - Lokalgruppen

Inga kommentarer.

=== Simon Gutgesell - Webbgruppen

Simon säger att kommunikation mellan honom och styrelsen fungerar bra eftersom han är en del av styrelsen.
Han jämför att vara med i styrelsen samt webbansvarig med styrelseposten webbansvarig från förra året som lika.
Han tror att det kan bli svårare att ansvara över hemsidan i framtiden beroende på om nästa webbansvarig kommer till meetups eller inte, men han säger att det är en fördel att separera på webbansvaret från att vara en del av styrelsen.

=== Emanuel Särnhammar - Meetupgruppen

Emanuel säger att det har varit en svårighet att vara meetupansvarig eftersom han har en schemakrock under meetups. Det har fungerat bra att ha eget ansvar över fikat.

=== Övrigt

En fråga ställs till Martin om hur han tycker att det fungerar att ansvara över game jam utan att vara en del av styrelsen.
Han svarar att det är skönt för att han slipper vara delaktig i styrelsenmöten och att det fungerar bra i största drag, men tycker att det kan vara lite svårt att kommunicera med styrelsen. Han tycker att det är bra att det inte är en styrelsepost, utan nackdelar om man löser kommunikationen med styrelsen.

== Hamzas stadgändringsförslag

Hamza föreslår att ta upp ett ärende om att göra en stadgändring. 10 röster för, det går igenom.

Hamza presenterar sitt stadgändringsförslag. Han säger att verksamhetsansvarig saknar poäng och inte borde finnas, att det skulle räcka med en vanlig ledamot istället.

Mötet kommenterar att verksamhetsansvarig borde ansvara för undergrupper enligt, i enlighet med förra mötets beslut om undergrupper. Det borde vara en röst för undergrupper under styrelsemöten.

Mötet motiverar existensen av verksamhetsansvarig.
- Ordförande borde inte vara ansvarig för allting
- Vikt ska ligga på verksamhetsansvarig för evenemang, verksamhetsansvarig bör ansvara för undergrupper
- Simon poängterar att styrelsen och undergrupper har upplevt svårigheter med kommunikationen, verksamhetsansvarige borde underlätta med det
- Victor säger att det kan vara svårt att vara verksamhetsansvarig när alla i styrelseledamoter och gruppledare känner varandra, för då kan kommunikationen ske direkt mellan styrelsen och undergruppsledaren. Om det skedde genom verksamhetsansvarige skulle det kännas onödigt byrokratiskt
- Hamza tycker inte att det är ett måste att ha en verksamhetsansvarig
- Frans säger att det historiskt sett har funnits en verksamhetsansvarig för att föreningen måste hålla i verksamhet, och då behöver någon ha ansvar för att verksamhet sker. Kan göras genom att delegera

Röstlängden justeras till 12 personer.

- Viktor tycker att stadgändringar inte borde ske spontant och känslomässigt, utan borde planeras under en längre tid

Mötet röstar om förslaget. 1 röst för, det går inte igenom.

= Mötet avslutas

Mötet avslutas 19:43.

#table(
    columns: (1fr, 1fr),
    stroke: none,
    inset: 0pt,
    gutter: 0.5em,
    v(4em), [],
    [*Mötesordförande*], [*Mötessekreterare*],
    [Henry Andersson], [Emanuel Särnhammar],
    v(4em), [],
    [*Justeringsperson*], [*Justeringsperson*],
    [Hannah Bahrehman], [Hamza Keifo],
)
